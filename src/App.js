import React, {Component} from 'react';
import './App.css';
import Modal from "./components/UI/Modal/Modal";
import Alert from "./components/UI/Alert/Alert";

class App extends Component {
  state = {
    modalDisplay: false,
    alertDisplay: false
  };

  toggleModal = () => {
    this.setState({modalDisplay: !this.state.modalDisplay});
  };

  continued = () => {
    alert('u clicked continued')
  };

  closeAlert = () => {
    this.setState({alertDisplay: false})
  };

  showAlert = () => {
    this.setState({alertDisplay: true})
  };

  render() {
    return (
      <div className="App">
        <Modal show={this.state.modalDisplay} closed={this.toggleModal} btnControls={[
          {type: 'Primary', label: 'Continue', clicked: this.continued},
          {type: 'Danger', label: 'Close', clicked: this.toggleModal}
        ]} title={"Some kinda modal title"}>
          This is modal content
        </Modal>
        <button onClick={this.toggleModal}>SHOW MODAL</button>
        <Alert type="info" close={this.closeAlert} display={this.state.alertDisplay}>This is alert content</Alert>
        <button onClick={this.showAlert}>SHOW ALERT</button>
      </div>
    );
  }
}

export default App;
