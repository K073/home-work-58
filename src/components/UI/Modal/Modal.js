import React from 'react';
import './Modal.css';
import Wrapper from "../../../hoc/Wrapper";
import Backdrop from "../Backdrop/Backdrop";
import IconClose from "../../../assets/ic_close.png";
import Button from "../Button/Button";

const Modal = props => {
  const modalFooter = props.btnControls ?
    <div className="Modal__footer">{props.btnControls.map(value => <Button type={value.type}
                                                                           clicked={value.clicked}>{value.label}</Button>)}</div> : null;
  return (
    <Wrapper>
      <Backdrop show={props.show} clicked={props.closed}/>
      <div className="Modal" style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0',
      }}>
        <div className="Modal__header">
          <h3 className="Modal__title">{props.title}</h3>
          {props.closed ? <img className="Modal__close" src={IconClose} onClick={props.closed} alt=""/> : null}
        </div>
        {props.children ? <p className="Modal__content">{props.children}</p> : null}
        {modalFooter}
      </div>
    </Wrapper>
  );
};

export default Modal;