import React from 'react';
import './Alert.css';
import IconClose from '../../../assets/ic_close.png';

const Alert = props => (
  <div hidden={!props.display} className={["Alert", props.type, props.close ? "dismissable": null].join(" ")}>
    {props.close ? <img className="Alert__close" onClick={props.close} src={IconClose} alt="" /> : null}
    {props.children}
  </div>
);

export default Alert;